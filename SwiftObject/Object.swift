//
//  Object.swift
//  SwiftObject
//
//  Created by Zach Eriksen on 6/6/18.
//  Copyright © 2018 Zach Eriksen. All rights reserved.
//

import Foundation

precedencegroup ObjectSetVariablePrecedence {
    higherThan: BitwiseShiftPrecedence
    associativity: right
}
precedencegroup ObjectGetVariablePrecedence {
    higherThan: ObjectSetVariablePrecedence
    associativity: right
}
precedencegroup ObjectRunePrecedence {
    higherThan: ObjectGetVariablePrecedence
    associativity: left
}
// Variables
infix operator <~~: ObjectSetVariablePrecedence   // Set named
infix operator ~~>: ObjectGetVariablePrecedence   // Get named
postfix operator ~~> // Get ""
prefix operator ~~>
// Functions
infix operator <^: ObjectSetVariablePrecedence   // Set named
infix operator ^>: ObjectGetVariablePrecedence   // Get named
postfix operator ^> // Get ""
// Run Functions
infix operator <^>: ObjectRunePrecedence  // With Any paramater
infix operator <>: ObjectRunePrecedence   // With no paramter
postfix operator <>
infix operator <~>: ObjectRunePrecedence  // With an Internally named variable
postfix operator <~>
// typealias
public typealias ObjectFunction = (Any?) -> Any?
public typealias ObjectVariable = Any
/// Attempt to make a simple JavaScript-like Object in Swift
@dynamicMemberLookup // Allows dot notation for "String" keys
public class Object {
    /// Grab ObjectVariables, under the type "String" key, with dot notation
    subscript(dynamicMember member: String) -> Box {
        return self ~~> member
    }
    subscript<T>(dynamicMember member: String) -> T {
        return (self ~~> member).pop()
    }
    
    /// Helper type to return inside functions
    public enum Status: String, Codable {
        public typealias RawValue = String
        /// Status returned when incorrect type is passed
        case TypeError
        /// Status returned when there is no value found
        case NilValue
        /// Status returned when there is no function found
        case NilFunction
        /// Status returned when function passed as expected
        case Completed
        /// Quick access to Status
        public static prefix func ~~>(left: Status) -> Status {
            return left
        }
    }
    /// Functions of the object
    fileprivate var functions: [AnyHashable: ObjectFunction] = [:]
    /// Variables of the object
    fileprivate var variables: [AnyHashable: Box] = [:]
    // MARK: Default Init
    /// Default init used to create a basic Object
    public init() {}
    // MARK: Private Helpers
    /// Retrieve a Function from the current object
    fileprivate func function(named: AnyHashable = "") -> ObjectFunction {
        return functions[named] ?? { _ in ~~>.NilFunction }
    }
    /// Retrieve a Value from the current object
    fileprivate func variable(named: AnyHashable = "") -> Box {
        return variables[named] ?? Box(~~>.NilValue)
    }
    /// Add a Value with a name to the current object
    fileprivate func addVariable(named: AnyHashable = "", value: ObjectVariable) {
        variables[named] = Box(value)
    }
    /// Add a Function with a name and a closure to the current object
    fileprivate func addFunction(named: AnyHashable = "", value: @escaping ObjectFunction) {
        functions[named] = value
    }
    /// Run a Function with or without a value
    fileprivate func runFunction(named name: AnyHashable = "", value: ObjectVariable = ~~>.NilValue) -> ObjectVariable? {
        return function(named: name)(value)
    }
    /// Run a Function with a internal value
    fileprivate func runInternalFunction(named name: AnyHashable = "", withInteralValueName iValueName: AnyHashable = "") -> ObjectVariable? {
        return function(named: name)(variable(named: iValueName).ship())
    }
}

// MARK: Custom Operator Functions / Runes
extension Object {
    // MARK: Variable Runes
    /// Add a Value with a name to the current object
    public static func <~~(left: Object, right: [AnyHashable: ObjectVariable]) {
        right.keys.forEach{ left.addVariable(named: $0, value: right[$0] ?? ~~>.NilValue) }
    }
    /// Shortcut to add a Value to the current object
    public static func <~~(left: Object, right: ObjectVariable) {
        if let right = right as? Dictionary<AnyHashable, ObjectVariable> {
            left <~~ right
        } else {
            left.addVariable(value: right)
        }
    }
    /// Retrieve a Value from the current object
    public static func ~~>(left: Object, right: ObjectVariable) -> Box {
        guard let right = right as? AnyHashable else {
            return Box(~~>.NilValue)
        }
        return left.variable(named: right)
    }
    /// Retrieve a Value from the current object and casts it to the type provided
    public static func ~~><E>(left: Object, right: ObjectVariable) -> E? {
        guard let right = right as? AnyHashable else {
            return nil
        }
        return (left ~~> right).open()
    }
    /// Retrieve the single Value of the current object and casts it to the type provided
    public static postfix func ~~><E>(left: Object) -> E? {
        return left~~>.open()
    }
    // MARK: Postfix Variable Runes
    /// Retrieve the single Value of the current object
    public static postfix func ~~>(left: Object) -> Box {
        return left.variable()
    }
    // MARK: Function Runes
    /// Add a Function with a name and a closure to the current object
    public static func <^(left: Object, right: [AnyHashable: ObjectFunction]) {
        right.keys.forEach{ left.addFunction(named: $0, value: right[$0] ?? { _ in ~~>.NilFunction }) }
    }
    /// Add a single Function to the current object
    public static func <^(left: Object, right: @escaping ObjectFunction) {
        left.addFunction(value: right)
    }
    /// Retrieve a Function from the current object
    public static func ^>(left: Object, right: ObjectVariable) -> ObjectFunction {
        guard let right = right as? AnyHashable else {
            return { _ in ~~>.NilFunction }
        }
        return left.function(named: right)
    }
    /// Retrieve the single Value of the current object
    public static postfix func ^>(left: Object) -> ObjectFunction {
        return left.function()
    }
    // MARK: Run Function Runes
    /// Run the single Function or named Function with an ObjectVariable
    public static func <^>(left: Object, right: ObjectVariable) -> ObjectVariable? {
        guard let tuple = right as? (AnyHashable, ObjectVariable) else {
            return left.runFunction(value: right)
        }
        return left.runFunction(named: tuple.0, value: tuple.1)
    }
    /// Run a Function without an ObjectVariable
    public static func <>(left: Object, right: ObjectVariable) -> ObjectVariable? {
        guard let right = right as? AnyHashable else {
            return ~~>.NilFunction
        }
        return left.runFunction(named: right)
    }
    /// Run the single Function or named Function with an interal ObjectVariable
    public static func <~>(left: Object, right: ObjectVariable) -> ObjectVariable? {
        guard let tuple = right as? (AnyHashable, AnyHashable) else {
            guard let name = right as? AnyHashable else {
                return ~~>.NilFunction
            }
            return left.runInternalFunction(withInteralValueName: name)
        }
        return left.runInternalFunction(named: tuple.0, withInteralValueName: tuple.1)
    }
    // MARK: Postfix Function Runes
    /// Run the single Function with an interal ObjectVariable
    public static postfix func <~>(left: Object) -> ObjectVariable? {
        // runFunction(Void) -> SingleFunction(SingleVariable)
        return left.runInternalFunction()
    }
    /// Run the single Function of the current Object, without an ObjectVariable'
    public static postfix func <>(left: Object) -> ObjectVariable? {
        return left.runFunction()
    }
}

extension Object: Equatable {
    /// Checks if the two objects are equals
    public static func == (lhs: Object, rhs: Object) -> Bool {
        return lhs.variables.keys.flatMap{ "\($0)" } == rhs.variables.keys.flatMap{ "\($0)" } &&
            lhs.functions.keys.flatMap{ "\($0)" } ==
            rhs.functions.keys.flatMap{ "\($0)" }
    }
}
