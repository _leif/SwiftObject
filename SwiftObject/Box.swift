//
//  Box.swift
//  SwiftObject
//
//  Created by Zach Eriksen on 7/5/18.
//  Copyright © 2018 Zach Eriksen. All rights reserved.
//

import Foundation
/// A wrapper for type Any
public struct Box: Equatable {
    private var value: Any
    private var kind: Any.Type
    // MARK: Default Init
    /// Default init used to create a basic Box
    public init(_ obj: Any) {
        value = obj
        kind = type(of: obj)
    }
    /// Replace the current object with another object
    /// NOTE: Setting the Box to itself will wrap itself
    public mutating func set(_ obj: Any) {
        value = obj
        kind = type(of: obj)
    }
    /// Returns the value of type Any
    public func ship() -> Any {
        return value
    }
    /// Returns the value of the type provided as an optional
    public func open<E>() -> E? {
        return value as? E
    }
    /// Returns the value of the type provided force unwrapped
    /// NOTE: Force Unwrap used
    public func pop<E>() -> E {
        return open()!
    }
    /// Check if the types and values are equal
    public static func == (lhs: Box, rhs: Box) -> Bool {
        return lhs.kind == rhs.kind && "\(lhs.ship())" == "\(rhs.ship())"
    }
}
