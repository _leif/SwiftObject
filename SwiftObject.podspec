Pod::Spec.new do |s|
# 1
s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "SwiftObject"
s.summary = "JS inspired Swift Object"
s.requires_arc = true

# 2
s.version = "0.4.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4
s.author = { "Zach Eriksen" => "zmeriksen@gmail.com" }

# 5
s.homepage = "https://gitlab.com/_leif/SwiftObject"

# 6
s.source = { :git => "https://gitlab.com/_leif/SwiftObject.git", :tag => "#{s.version}"}

# 7
s.framework = "Foundation"

# 8
s.source_files = "SwiftObject/**/*.{swift}"
end
