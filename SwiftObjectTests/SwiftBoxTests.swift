//
//  SwiftBoxTests.swift
//  SwiftObjectTests
//
//  Created by Zach Eriksen on 7/5/18.
//  Copyright © 2018 Zach Eriksen. All rights reserved.
//

import XCTest
@testable import SwiftObject

class SwiftBoxTests: XCTestCase {
    var box: Box!
    
    override func setUp() {
        super.setUp()
        box = Box("INIT VALUE")
    }
    
    override func tearDown() {
        box = nil
        super.tearDown()
    }
    
    func testBoxPop() {
        // Force unwrap as a String
        let first: String = box.pop()
        // Set the value to 123 Int
        box.set(123)
        let second: Int = box.pop()
        box.set(box)
        let third: Box = box.pop()
        let forth: Int = third.pop()
        
        XCTAssert(first == "INIT VALUE")
        XCTAssert(forth == second)
        XCTAssert(third != box)
    }
    
    func testBoxOpen() {
        let first = box.open() ?? ""
        
        box.set(123)
        let second = box.open() ?? -2
        box.set(box)
        let third: Box = box.open()!
        let forth = third.open() ?? -4
        
        XCTAssert(first == "INIT VALUE")
        XCTAssert(forth == second)
        XCTAssert(third != box)
    }
}
