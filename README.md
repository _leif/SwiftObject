# SwiftObject


### Creating an Object
```
var obj = Object()
```
### Example Variables

```
func testVariableExample1() {
    // Assign the Object's Single Variable
    obj <~~ "Hello World"
    // Assign four different type keys Variables with values
    obj <~~ ["Hello World": 123,
    5: "Hi",
    3.14: "pi",
    UIColor.blue: "blue"]

    // Check that the Object's Single Variable is "Hello World"
    XCTAssert("\(obj~~>)" == "Hello World")
    // Cast the Variable Hello World to Int and checks that it is 123
    XCTAssert(obj ~~> ("Hello World", Int.self) ?? 0 == 123)
    // Cast the Variable 5 to String and checks that it is "Hi"
    XCTAssert(obj ~~> (5, String.self) ?? "" == "Hi")
    // Cast the Variable 3.14 to String and checks that it is "pi"
    XCTAssert(obj ~~> (3.14, String.self) ?? "" == "pi")
    // Cast the Variable UIColor.blue to String and checks that it is "blue"
    XCTAssert(obj ~~> (UIColor.blue, String.self) ?? "" == "blue")
}

func testVariableExample2() {
    // Assign four Variables with different value types
    obj <~~ ["pi": 3.14,
            "foo": "bar",
            "childObj": Object(),
            "count": 0]

    // Assign the Variable childObj's Single Variable to 135
    (obj ~~> ("childObj", Object.self))! <~~ 135

    // guard unwrap of the Variables with as? Type style
    guard let pi = obj ~~> "pi" as? Double,
            let foo = obj ~~> "foo" as? String,
            let childObj = obj ~~> "childObj" as? Object,
            let childObjValue = childObj~~> as? Int,
            let count = obj ~~> "count" as? Int else {
                XCTFail()
                return
    }

    // Check values
    XCTAssert(pi == 3.14)
    XCTAssert(foo == "bar")
    XCTAssert(childObjValue == 135)
    XCTAssert(count == 0)
}

func testVariableExample3() {
    // Assign four Variables with different value types
    obj <~~ ["pi": 3.14,
            "foo": "bar",
            "childObj": Object(),
            "count": 0]

    // Assign the Variable childObj's Single Variable to 136
    (obj ~~> ("childObj", Object.self))! <~~ 136

    // guard unwrap of the Variables with <E.Type> Style
    guard let pi = obj ~~> ("pi", Double.self),
            let foo = obj ~~> ("foo", String.self),
            let childObj = obj ~~> ("childObj", Object.self),
            let childObjValue = childObj ~~> Int.self,
            let count = obj ~~> ("count", Int.self) else {
                XCTFail()
                return
    }

    // Check values
    XCTAssert(pi == 3.14)
    XCTAssert(foo == "bar")
    XCTAssert(childObjValue == 136)
    XCTAssert(count == 0)
}

func testVariableExample4() {
    // Assign Variables
    obj <~~ ["a": 1,
            "child": Object(),
            "b": 2]

    // Check values
    XCTAssert(obj ~~> ("a", Int.self) == 1)
    XCTAssert(obj ~~> ("b", Int.self) == 2)

    // Unwrap as Object
    guard let child = obj ~~> ("child", Object.self) else {
        XCTFail()
        return
    }
    // Store info into a local var
    child <~~ ["hello": "world",
                "a": 2]
    // Retrieve child from parent
    guard let pchild = obj ~~> ("child", Object.self) else {
        XCTFail()
        return
    }
    // Check values
    XCTAssert(pchild ~~> ("a", Int.self) == 2)
    XCTAssert(pchild ~~> ("hello", String.self) == "world")
}

func testVariableExample5() {
    // Add the Object to the Object's Single Variable
    obj <~~ obj

    // Grab obj's Single Variable
    let one = obj ~~> Object.self ?? Object()
    // Grab one's Single Variable
    let two = one ~~> Object.self ?? Object()
    // Grab two's Single Variable
    let three = two ~~> Object.self ?? Object()
    let n = 100
    // Grab n's Single Variable
    var n_obj: Object = obj
    (1 ... n).forEach { i in
        n_obj = (n_obj ~~> Object.self)!
    }
    // Check values
    XCTAssert(one == two &&
                two == three)
    XCTAssert(obj == n_obj &&
                one == n_obj &&
                two == n_obj &&
                three == n_obj)
}
 ```   
### Example Functions

 ```
func testFunctionExample1() {
        obj <^ ["isEven": { value in
                guard let i = value as? String else {
                return "ERROR: Could not parse \(value) to type String"
                }
                return i.count % 2 == 0
        }]
 func testFunctionExample1() {
     // Create function isEven(value: String) -> Bool
     obj <^ ["isEven": { value in
         // Cast value as String
         guard let i = value as? String else {
             // Return TypeError
             return ~~>.TypeError
         }
         // Check if i is even
         return i.count % 2 == 0
     }]
     
     // Assign the function isEven to a local variable named isEven
     let isEven = obj ^> "isEven"
     
     // Check values
     XCTAssert(isEven(12345) is Object.Status)
     XCTAssertFalse(isEven("Hello World") as! Bool)
     XCTAssertFalse(isEven("12345") as! Bool)
     XCTAssertFalse(isEven("1") as! Bool)
     XCTAssert(isEven("1234") as! Bool)
     XCTAssert(isEven("") as! Bool)
 }

        let isEven = obj ^> "isEven"

        XCTAssert("\(isEven(12345))".contains("ERROR:"))
        XCTAssertFalse(isEven("Hello World") as! Bool)
        XCTAssertFalse(isEven("12345") as! Bool)
        XCTAssertFalse(isEven("1") as! Bool)
        XCTAssert(isEven("1234") as! Bool)
        XCTAssert(isEven("") as! Bool)
}
func testFunctionExample2() {
    // Assign Variables
    obj <~~ 1000
    obj <~~ ["sum": 0]

    // Assigning the Object's Single Function (Int) -> Int
    obj <^ { value in
        // Cast value to Int and grab obj's Single Variable
        guard let i = value as? Int,
            let inc = self.obj ~~> Int.self else {
            return ~~>.TypeError
        }
        return i + inc
    }

    // Assign the Single Function to a local variable named add
    let add = obj^>

    // Assign sum to the output of add(5)
    obj <~~ ["sum": add(5)]

    // Check values
    XCTAssert(obj ~~> ("sum", Int.self) ?? 0 == 1005)
    XCTAssert(obj <~> ("", "sum") as! Int == 2005)
}

func testFunctionExample3() {
    // Assign Variables
    obj <~~ "xyz"
    obj <~~ [56: "SwiftObject",
    UIColor.orange: 133]

    // Assign Function named sayHello
    obj <^ ["sayHello": { _ in
        return "Hello World"
    }]

    // Assign three different Functions
    obj <^ [100: { _ in 100 },
            UIColor.yellow: { iValue in "[\(iValue ?? "")]" },
            "color": { _ in UIColor.black }]
    // Assign Single Function
    obj <^ { v in 27 }

    // Check values
    XCTAssert(obj <^> (UIColor.yellow, "YELLOW") as? String ?? "" == "[YELLOW]")
    XCTAssert(obj <> "color" as? UIColor ?? .clear == .black)
    XCTAssert(obj <~> (UIColor.yellow, UIColor.orange) as? String ?? "" == "[\(133)]")
    XCTAssert(obj <~> UIColor.orange as? Int ?? 0 == 27)
    XCTAssert(obj <> 100 as? Int ?? 0 == 100)
    XCTAssert("\(obj <> "sayHello" ?? "")" == "Hello World")
}

func testFunctionExample4() {
    // Assign Single Variable to 0
    obj <~~ 0
    // Create Functions
    obj <^ ["add": { i in (self.obj ~~> Int.self)! + (i as? Int)!},
    "sub": { i in (self.obj ~~> Int.self)!  - (i as? Int)!}]
    // Update Single Variable
    obj <~~ obj <^> ("add",23)
    obj <~~ obj <^> ("add",343)
    obj <~~ obj <^> ("sub",421)
    // Check final value
    XCTAssert(obj ~~> Int.self == -55)
}

func testFunctionExample5() {
    // Assign Single Variable to 0
    obj <~~ 0
    // Assign Single Function
    obj <^ { inc in
        return self.obj <~~ ((self.obj ~~> Int.self)! + (inc as! Int))
    }
    let inc = obj^>
    _ = inc(5)
    _ = inc(8)
    _ = inc(3)
    // Using Run Runes
    // run Single Function with the Single Variable passed as a parameter
    _ = obj<~>
    // run Single Function with different Int values
    _ = obj <^> 6
    _ = obj <^> 4

    // Check values
    XCTAssert(obj ~~> Int.self == 42)
}

// Recursion Test
func testFunctionExample6() {
    // Assign Variable beersOnTheWall to 99
    obj <~~ ["beersOnTheWall": 99]
    // Assign Single Function
    obj <^ { _ in
        // Unwrap beersOnTheWall
        guard let beersOnTheWall = self.obj ~~> ("beersOnTheWall", Int.self) else {
            XCTFail()
            return nil
        }
        // Check if beersOnTheWall is 0
        if beersOnTheWall == 0 {
            // End Recursion
            return "Oops no more beer on the wall!"
        }
        // Assign beersOnTheWall to beersOnTheWall - 1
        self.obj <~~ ["beersOnTheWall": beersOnTheWall - 1]
        // Start Recursion
        return self.obj<>
    }
    // Check that beersOnTheWall is 99
    XCTAssert(obj ~~> ("beersOnTheWall", Int.self) == 99)
    // Call Single Function
    _ = obj<>
    // Check that beersOnTheWall is 0
    XCTAssert(obj ~~> ("beersOnTheWall", Int.self) == 0)
}

func testA() {
    // Assign Single Function (Int) -> [Int]
    obj <^ { i in
        // Unwrap as Int
        guard let j = i as? Int else {
            return ~~>.TypeError
        }
        // Create [Int] of length j + 1
        return (0 ... j).map({ $0 + ($0+j)} )
    }
    // Create local variable of the Single Function
    let splat = obj^>
    // Unwrap as [Int]
    guard let value = splat(5) as? [Int] else {
        XCTFail()
        return
    }
    // Check values
    XCTAssert(value == [5, 7, 9, 11, 13, 15])
}

func testB() {
    // Assign Variable
    obj <~~ ["apple": 123]
    // Assign Single Function
    obj <^ { i in print(i) }

    // Use Runes
    _ = obj <^> "Hello World"

    _ = obj<>

    _ = obj <~> "apple"
    // Quick Unwrap value
    let value: Int = obj ~~> ("apple", Int.self) ?? 0
    // Check value
    XCTAssert(value == 123)
}

func testC() {
    // Object Builder
    obj <^ { _ in
        // Assign Single Variable to 0
        self.obj <~~ 0
        // Create Function inc (Int) -> Int
        self.obj <^ ["inc": { i in
            guard let i = i as? Int else {
                return ~~>.TypeError
            }
            return i + 1
        }]
        // Assign the Single Function to a new function
        self.obj <^ { _ in
            return self.obj <~~ self.obj <~> ("inc","") ?? -1
        }
        // Return a Completed Function Status
        return ~~>.Completed
    }
    // Run the Single Function 11 times
    _ = (0 ... 9).map({ _ in obj<> })
    _ = obj<~>
    // Check values
    XCTAssert(obj ~~> Int.self == 10)
}
```

Thanks for the amazing docs [Jazzy!](https://github.com/realm/jazzy)
